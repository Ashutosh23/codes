var express  = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var expressValidator = require('express-validator');
var mongojs = require('mongojs');
var db = mongojs('customerapp', ['users']);
var ObjectId = mongojs.ObjectId;
var app = express();

/*
var logger = function(req, res, next){  //every time you refresh
	console.log('Logging...');
	next();
}

app.use(logger);
*/

//View Engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname,'views'));
//body parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//set static path
app.use(express.static(path.join(__dirname, 'public')));

//global vars
app.use(function(req, res, next){
	res.locals.errors = null;
	next();
});
/*
var people = [
	{
		name:'Jeff',
		age:30
	},
	{
		name:'Jasmine',
		age:20
	},
	{
		name:'Jeremy',
		age:10
	}
]
*/
app.use(expressValidator({
	errorFormatter: function(param, msg, value, location) {
		var namespace = param.split('.')
		, root   =  namespace.shift()
		, formParam = root;

		while(namespace.length) {
			formParam += '[' + namespace.shift() + ']';
		}
		return {
			param : formParam,
			msg   : msg,
			value : value,
			location: location
		};
	}
}));

var users = [
	{
		id: 1,
		first_name: 'john',
		last_name: 'doe',
		email: 'johndoe@gmail.com'
	},
	{
		id: 2,
		first_name: 'johnson',
		last_name: 'doe',
		email: 'johnsondoe@gmail.com'
	},
	{
		id: 3,
		first_name: 'johanna',
		last_name: 'doe',
		email: 'johannadoe@gmail.com'
	}
]

app.get('/',function(req, res){
	db.users.find(function(err, docs){
		//console.log(docs);
		res.render('index', {
		title: 'Customers',
		users: docs
	});
	})
	//res.json(people);
	//res.send('hello');
	//res.render('index');
});

app.post('/users/add', function(req,res){
	//console.log('Form submitted');
	//console.log(req.body.first_name);
	req.checkBody('first_name', 'firstname is required').notEmpty();
	req.checkBody('last_name', 'lastname is required').notEmpty();
	req.checkBody('email', 'email is required').isEmail();

	var errors  = req.validationErrors();

	if(errors){
		res.render('index', {
		title: 'Customers',
		users: users,
		errors: errors
	});
	}
	else{
		var newUser = {
		first_name: req.body.first_name,
		last_name: req.body.last_name,
		email: req.body.email
	}

	db.users.insert(newUser, function(err, res){
		if(err){
			console.log(err);
		}
		app.get('/', function(request,respond) {
      		respond.redirect('/'); //Pass between the brackets your URL.
    	});
	});
	//console.log('SUCCESS');
	}
 });

app.delete('/users/delete/:id', function(req, res){
	db.users.remove({_id: ObjectId(req.params.id)}, function(err, result){
		if(err){
			console.log(err);
		}
		res.redirect('/');
	});
});

app.listen(3020,function(){
	console.log('server started on Port 3020...');
})
