const express = require('express');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const nodemailer = require('nodemailer');
const path = require('path');

const app = express();

//load view engine
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

// body parser middleware

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Set public folder
app.use('/public', express.static(path.join(__dirname, 'public')));


app.get('/', (req,res) => {
    res.render('contact');
});

app.post('/send', (req, res) => {
    const output = `
        <p>You have a new contact request</p>
        <h3>Contact Details</h3>
        <ul>
            <li>Name: ${req.body.name}<li>
            <li>Company: ${req.body.company}<li>
            <li>Email: ${req.body.email}<li>
            <li>Phone: ${req.body.phone}<li>
        </ul>
        <h3>Message</h3>
        <p>${req.body.message}</p>
    `;

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        //host: 'smtp.ethereal.email',
        //port: 587,
        service: 'gmail',
        //secure: false, // true for 465, false for other ports
        auth: {
            user: 'goyal0601@gmail.com', // generated ethereal user
            pass: 'ashu@5555'  // generated ethereal password
        },
        tls:{
            rejectUnauthorized:false,
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"Nodemailer contact" <goyal0601@gmail.com>', // sender address
        to: 'goyal0601@gmail.com', // list of receivers
        subject: 'Node contact request', // Subject line
        text: 'Hello world?', // plain text body
        html: output // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        res.render('contact', {msg:'email has been sent'});

        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    });
});

// start server
app.listen(3010, function(){
    console.log('server started on 3010');
});